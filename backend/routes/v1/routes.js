//
const MainController = require('../../src/controllers/mainController.js')

// Routes Declaration
async function registeredRoutes(fastify) {
    //@TODO: Add healthchecking route
    fastify.get('/hi', (request, reply) => {
        reply.send({ hello: 'world' })
    })

    //
    fastify.get('/get_crypto_currencies_data', MainController.getCryptoCurrenciesData);
}

module.exports = registeredRoutes;