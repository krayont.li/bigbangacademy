//
const CoinLayer = require('../helpers/coinLayer.js');

module.exports = {
    //
    fetchData: async ({symbols, target_currency}) => {
        //
        switch (process.env.CRYPTO_SOURCE.toLocaleLowerCase()) {
            case 'coin_layer':
            default: {
                return await CoinLayer.fetch(symbols, target_currency);
            }
        }

    }
}
