//
const CryptoProvider = require('../services/cryptoProvider.js');

//
module.exports = {
    getCryptoCurrenciesData: async (request) => {
        // @TODO: Validate incoming request
        const target_currency = request.query?.currency ?? 'USD';
        // @TODO: Dynamic Symbols and Validation
        const symbols = 'BTC,ETH,LTC,XMR,XRP,DOGE,DASH';

        return await CryptoProvider.fetchData({
            symbols: symbols,
            target_currency: target_currency
        });
    }
}
