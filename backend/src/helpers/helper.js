//
module.exports = {
    //
    setupResponse: (success, symbols, target_currency, data) => {
        if (success) {
            const array = symbols.split(',');

            if (array.length < 1 || array[0] === "") {
                return {
                    success: false,
                    currency: target_currency,
                    rates: {}
                }
            }

            let response = {};
            for (const elem of array) {
                if (data[elem]) {
                    response[elem] = data[elem];
                } else {
                    response[elem] = {};
                }
            }

            return {
                success: true,
                currency: target_currency,
                rates: response
            }
        } else {
            return {
                success: false,
                currency: target_currency,
                rates: data
            }
        }
    }
}