//
const axios = require('axios');
const redisClient = require('./redis.js');
const Helper = require('./helper.js');

//
module.exports = {
    //
    fetch: async (symbols, target_currency) => {
        //
        let data = null;

        //
        try {
            data = JSON.parse(await redisClient.get(`coin_layer_${target_currency.toLocaleLowerCase()}`)) ?? null;
        } catch (err) {
            // @TODO: Log Error;
        }

        if (data === null) {
            //
            try {
                const url = `${process.env.COIN_LAYER_URL}?access_key=${process.env.COIN_LAYER_API_KEY}&target=${target_currency}&expand=1`;
                const response = await axios.get(url);

                if (response.data?.success === true) {

                    data = response.data;
                    try {
                        await redisClient.set(
                            `coin_layer_${target_currency.toLocaleLowerCase()}`,
                            JSON.stringify(data) ?? JSON.stringify({}),
                            'ex',
                            Number(process.env.REDIS_EXPIRY)
                        );
                    } catch (err) {
                        // @TODO: Log Error;
                    }
                }

            } catch (err) {
                // @TODO: Log Error
            }
        }

        if (data !== null) {
            return Helper.setupResponse(data.success, symbols, target_currency, data.rates);
        }

        //
        return {
            success: false,
            target: target_currency,
            data: {}
        };
    }
}
