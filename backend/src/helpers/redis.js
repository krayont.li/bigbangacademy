const Redis = require("ioredis");
require('dotenv').config();

const redisClient = new Redis({
    port: Number(process.env.REDIS_PORT) || 6379,
    host: process.env.REDIS_HOST || '127.0.0.1',
    password: process.env.REDIS_PASSWORD || '',
    db: Number(process.env.REDIS_DB) || 1,
});

module.exports = redisClient;
