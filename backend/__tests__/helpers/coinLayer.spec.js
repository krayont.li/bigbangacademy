const axios = require('axios');
const redisClient = require('../../src/helpers/redis.js');
const Helper = require('../../src/helpers/helper.js');
const CoinLayer = require('../../src/helpers/coinLayer.js');

jest.mock('axios');
jest.mock('../../src/helpers/redis.js');
jest.mock('../../src/helpers/helper.js');

describe('fetch', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should fetch data from redis if it exists', async () => {
        const symbols = 'BTC,ETH';
        const target_currency = 'USD';
        const redisData = {
            success: true,
            rates: {
                BTC: {
                    rate: 50000,
                },
                ETH: {
                    rate: 1500,
                }
            }
        };
        const expectedData = Helper.setupResponse(redisData.success, symbols, target_currency, redisData.rates);

        redisClient.get.mockResolvedValueOnce(JSON.stringify(redisData));

        const result = await CoinLayer.fetch(symbols, target_currency);

        expect(redisClient.get).toHaveBeenCalledWith('coin_layer_usd');
        expect(result).toEqual(expectedData);
    });

    //
    it('should fetch data from the API if it does not exist in redis', async () => {
        const symbols = 'BTC,ETH';
        const target_currency = 'USD';

        const apiData = {
            success: true,
            rates: {
                BTC: {
                    rate: 50000,
                },
                ETH: {
                    rate: 1500,
                }
            }
        };
        const expectedData = Helper.setupResponse(apiData.success, symbols, target_currency, apiData.rates);

        redisClient.get.mockResolvedValueOnce(null);
        axios.get.mockResolvedValueOnce({ data: apiData });

        const result = await CoinLayer.fetch(symbols, target_currency);

        expect(redisClient.get).toHaveBeenCalledWith('coin_layer_usd');
        expect(axios.get).toHaveBeenCalledWith(`${process.env.COIN_LAYER_URL}?access_key=${process.env.COIN_LAYER_API_KEY}&target=${target_currency}&expand=1`);
        expect(redisClient.set).toHaveBeenCalledWith('coin_layer_usd', JSON.stringify(apiData), 'ex', Number(process.env.REDIS_EXPIRY));
        expect(result).toEqual(expectedData);
    });

    //
    it('should catch and hanled redis errors', async () => {
        const symbols = 'BTC,ETH';
        const target_currency = 'USD';

        redisClient.get.mockRejectedValueOnce(new Error('Redis error'));

        const result = await CoinLayer.fetch(symbols, target_currency);

        expect(redisClient.get).toHaveBeenCalledWith('coin_layer_usd');
        expect(result).toEqual({ success: false, target: target_currency, data: {} });
    });
});
