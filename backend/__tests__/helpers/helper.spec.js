//
const Helper = require('../../src/helpers/helper');

describe('setupResponse function', () => {
    test('should return expected response when success is true and symbols are valid', async () => {
        const data = {
            BTC: {
                rate: 18629.396592,
            },
            ETH: {
                rate: 1304.030753,
            }
        };

        const response = Helper.setupResponse(true, 'BTC,ETH', 'AUD', data);

        expect(response.success).toBe(true);
        expect(response.currency).toBe('AUD');
        expect(response.rates).toEqual({
            BTC: {
                rate: 18629.396592,
            },
            ETH: {
                rate: 1304.030753,
            }
        });
    });

    test('should return expected response when success is true and symbols are empty', async () => {
        const data = {
            BTC: {
                rate: 18629.396592,
            },
            ETH: {
                rate: 1304.030753,
            }
        };
        const response = Helper.setupResponse(true, '', 'AUD', data);

        expect(response.success).toBe(false);
        expect(response.currency).toBe('AUD');
        expect(response.rates).toEqual({});
    });

    test('should return expected response when success is false', async () => {
        const data = {
            error: 'An error occurred',
        };
        const response = Helper.setupResponse(false, 'USD,EUR,GBP', 'AUD', data);

        expect(response.success).toBe(false);
        expect(response.currency).toBe('AUD');
        expect(response.rates).toEqual({
            error: 'An error occurred',
        });
    });

});
