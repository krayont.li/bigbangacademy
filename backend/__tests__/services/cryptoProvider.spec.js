//
const CoinLayer = require('../../src/helpers/coinLayer.js');

jest.mock('../../src/helpers/coinLayer.js');

//
describe('fetchData', () => {
  const symbols = 'BTC,ETH';
  const target_currency = 'USD';

    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('should call CoinLayer.fetch with correct arguments', async () => {
        await CoinLayer.fetch({ symbols, target_currency });
        expect(CoinLayer.fetch).toHaveBeenCalledTimes(1);
    });

    it('should return data from CoinLayer.fetch', async () => {
        const expectedData = {
            "success": true,
            "currency": "usd",
            "rates": {
                "BTC": {
                    "rate": 18629.396592,
                    "high": 18913.91787,
                    "low": 18414.125594,
                    "vol": 4884990018.37991,
                    "cap": 359708144547.5856,
                    "sup": 19308631,
                    "change": -52.93446299999778,
                    "change_pct": -0.2833397119672108
                },
                "ETH": {
                    "rate": 1304.030753,
                    "high": 1327.777325,
                    "low": 1286.879942,
                    "vol": 2098954670.103745,
                    "cap": 157164962406.87222,
                    "sup": 120522435.5678,
                    "change": 1.8727670000000671,
                    "change_pct": 0.1438202599173759
                }
            }
        };

        CoinLayer.fetch.mockResolvedValueOnce(expectedData);

        const result = await CoinLayer.fetch({ symbols, target_currency });

        expect(result).toEqual(expectedData);
    });

});
