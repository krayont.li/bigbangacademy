'use strict'
//
require('dotenv').config();

// Require the framework and instantiate it
const fastify = require('fastify')({
	logger: true
})

//
fastify.register(require('@fastify/cors'), {
  origin: true
})

// Rotues
fastify.register(require('./routes/v1/routes.js'), { prefix: '/api/v1' });


// Global Error Handler
fastify.setErrorHandler(async (error, request, reply) => {
    //
    return reply.status(500).send(reply);
});

// Run the server!
const start = async () => {
  	try {
    	await fastify.listen({ port: 3000, host: '0.0.0.0' })
  	} catch (err) {
		fastify.log.error(err)
		process.exit(1)
  	}
}
start()