# BigBangAcademy

# Info
  - This application is implemented based on the draft Architecture Design v1 (see arch_design_v1.pdf)
  - Architecture Design v2 can be reference to if scaling is needed

# Pre-Requisite
  1. Docker and Docker Compose

# Instruction
  1. Clone the project and ensure that port 8080, 3000, and 63799 are open and available on your computer
  2. Go to the root directory and run "docker-compose up -d"
  3. After building you will see 3 services/containers running on your docker (1 frontend, 1 backend, and 1 Redis)
  4. Open your Chrom web browser and enter http://localhost:8080


