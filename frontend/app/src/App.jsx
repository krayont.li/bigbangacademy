import { useState, useEffect } from 'react';
import './App.css'

//
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';

//
import Card from './components/card';

function App() {
	const [currencyData, setCurrencyData] = useState(null);
	const [currencyList, setCurrencyList] = useState(['USD', 'GBP', 'EUR', 'HKD', 'CNY']);

	useEffect(() => {
		// default to usd
	  	fetchData('USD');
	}, []);

	const fetchData = async (currency) => {
		const url = `${import.meta.env.VITE_BACKEND_URL}?currency=${currency}`;
		try {
			const response = await fetch(url);
			const data = await response.json();

			if (data.success) {
				setCurrencyData(data);
			} else {
				setCurrencyData(null);
			}
		} catch (err) {
			setCurrencyData(null);
		}
	}

	function handleCurrencySelectorChange(value) {
		// console.log(value.value);
		fetchData(value.value);
	}

	return (
		<div className="App">

			<div className="header">
				<h2>Cryptocurrency Realtime Price</h2>

				<Dropdown
					options={currencyList}
					onChange={handleCurrencySelectorChange}
					value={currencyList[0]}
					placeholder="Select an option"
				/>
			</div>

			<div className="wrapper">
				{ currencyData && Object.keys(currencyData.rates).map((row, index) => {
					const changeIncrease = currencyData.rates[row].change > 0;
					return (
						<Card
							key={index}
							title={row}
							price={`$${currencyData.rates[row].rate ?? '-'}`}
							volume={currencyData.rates[row].vol ?? '-'}
							change={currencyData.rates[row].change ?? '-'}
							increase={changeIncrease}
						/>
					);
				})}
			</div>

			{ currencyData === null && (
				<h3 className="error_msg">System currently not available. Please Try Again Later.</h3>
			)}
		</div>
	)
}

export default App
