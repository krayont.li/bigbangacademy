import React from 'react'
import ReactDOM from 'react-dom/client'
import { Helmet } from 'react-helmet';

import App from './App'
import './index.css'

ReactDOM.createRoot(document.getElementById('root')).render(
    <React.StrictMode>
        <Helmet>
            <meta charset="UTF-8" />
            <title>Cryptocurrency Realtime Price</title>
        </Helmet>
        <App />
    </React.StrictMode>,
)
