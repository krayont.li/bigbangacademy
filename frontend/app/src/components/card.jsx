import clsx from "clsx";

//
function Card( props ) {
    return (
        <div className="card">

            <div className="card-top">
                <h2>{props.title}</h2>
            </div>

            <div className="card-body">
                <h3>{props.price}</h3>
            </div>

            <div className="card-details">
                <span className="card-details-volumn">
                    <p>Volume:</p>
                    <p>{props.volume}</p>
                </span>
                <span className="card-details-change">
                    <p>Change:</p>
                    <p className={clsx(
                        props.increase ? 'rate_increase' : 'rate_decrease',
                    )}>
                        {props.change}
                    </p>
                </span>
            </div>
        </div>
    );
}

export default Card;