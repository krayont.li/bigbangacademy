import { defineConfig, loadEnv } from 'vite'
import react from '@vitejs/plugin-react-swc'
//
export default ({ mode }) => {
  process.env = {...process.env, ...loadEnv(mode, process.cwd())};

  return defineConfig({
    plugins: [react()],

    node: {
      extensions: ['.js', '.jsx', '.ts', '.tsx']
    },

    server: {
      port: parseInt(process.env.VITE_PORT) || 3081,
    },

    build: {
      outDir: './dist',
    },
  });
}
